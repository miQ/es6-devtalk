import React from 'react';
const USED_FEATURES = [
  'Generators (redux-saga)',
  'Promises (routes.js)',
  'classes (everywhere)',
  'arrow functions (everywhere)',
  'Inheritance (containers)',
  'modules (everywhere)',
  'default values (initial state)',
  'custom interpolation (styles - styled-components)'
];

const NOT_YET_USED = [
  'string repeating',
  'number comparision',
  'object property assignment',
  'number type checking',
  'destructing',
  'Internalization API'
];
export default class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const renderList = (list) => {
        return list.map((feature) => {
          return (
            <li key={feature}>{feature}</li>
          );
        });
    };
    return (
      <div>
        <h1>What we've done:</h1>
        <ul>
          {renderList(USED_FEATURES)}
        </ul>

        <h1>What we should do:</h1>
        <ul>
          {renderList(NOT_YET_USED)}
        </ul>
      </div>
    );
  }
}

