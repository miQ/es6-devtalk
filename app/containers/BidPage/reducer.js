import {REDUCER_TYPES} from './constants';
import { fromJS } from 'immutable';

// The initial state of the App
const initialState = fromJS({
  teams: [],
  loading: false,
  success: false,
  money: 500
});
export default function (state = initialState, actions) {
  switch (actions.type){
    case REDUCER_TYPES.LOAD_TEAMS:
      return state
        .set('loading', true);
    case REDUCER_TYPES.LOAD_TEAMS_SUCCESS:
        return state
          .set('success', true)
          .set('loading', false)
          .set('teams', actions.teams);
    case REDUCER_TYPES.BET:
      console.log(state.get('money'));
      return state
        .set('money', actions.betMoney);
    default:
      return state;
  }
}