import { createSelector } from 'reselect';

const selectBid = () => (state) => state.get('bid');

const selectTeams = () => createSelector(
  selectBid(),
  (bidState) => bidState.get('teams')
);

const selectLoadingTeams = () => createSelector(
  selectBid(),
  (bidState) => bidState.get('loading')
);

const selectLoadingSuccess = () => createSelector(
  selectBid(),
  (bidState) => bidState.get('success')
);

const selectMoney = () => createSelector(
  selectBid(),
  (bidState) => bidState.get('money')
);

export {
  selectBid,
  selectTeams,
  selectLoadingTeams,
  selectLoadingSuccess,
  selectMoney
};
