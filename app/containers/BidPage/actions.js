import {REDUCER_TYPES} from './constants';

export function loadTeams() {
  return {
    type: REDUCER_TYPES.LOAD_TEAMS
  }
}

export function teamsLoaded(teams) {
  return {
    type: REDUCER_TYPES.LOAD_TEAMS_SUCCESS,
    teams
  }
}

export function bet(money) {
  return {
    type: REDUCER_TYPES.BET,
    betMoney: money
  }
}