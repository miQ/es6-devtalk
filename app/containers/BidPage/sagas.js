import {put, call} from 'redux-saga/effects';
import {teamsLoaded} from './actions';
import request from 'utils/request';
export function* getTeams() {
  const requestURL = `http://localhost:3000/api/teams`;
  try {
    const data = yield call(request, requestURL);
    yield put(teamsLoaded(data.teams));
  } catch (err) {
    console.log(err);
  }
}

export default [
  getTeams
]