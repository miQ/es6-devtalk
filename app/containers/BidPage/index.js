import React from 'react';
import TournamentBracket from '../../components/TournamentBracket';
import {loadTeams, bet} from './actions';
import {connect} from 'react-redux';
import {selectTeams, selectLoadingTeams, selectLoadingSuccess, selectMoney} from './selectors';
import {createStructuredSelector} from 'reselect';
export class BidPage extends React.Component {
  componentDidMount() {
    this.props.loadTeams();
  }


  render() {
    const renderTournamentBracket = () => {
      if (this.props.teams && !this.props.loading && this.props.success) {
        return (
          <TournamentBracket rounds={this.props.teams}/>
        );
      }
    };
    return (
      <div>
        <h1>Let's bid!</h1>
        <h1>Your money: {this.props.money}$</h1>
        <button>Next round</button>
        <button onClick={() => {this.props.betMoney(100)}}>Bet</button>
        {renderTournamentBracket()}
      </div>
    )
  }
}


export function mapDispatchToProps(dispatch) {
  return {
    loadTeams: () => {
      dispatch(loadTeams());
    },
    betMoney: (money) => {
      dispatch(bet(money))
    }
  };
}

const mapStateToProps = createStructuredSelector({
  teams: selectTeams(),
  loading: selectLoadingTeams(),
  success: selectLoadingSuccess(),
  money: selectMoney()
});

export default connect(mapStateToProps, mapDispatchToProps)(BidPage);