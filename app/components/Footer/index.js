import React from 'react';
import { FormattedMessage } from 'react-intl';

import Wrapper from './Wrapper';
import messages from './messages';

function Footer() {
  return (
    <Wrapper>
      <section>
        <FormattedMessage
          {...messages.authorMessage}
          values={{
            author: <p>Michał Przyszczypkowski, Artur Kudeł, Kaj Białas, Daniel Capeletti</p>,
          }}
        />
      </section>
    </Wrapper>
  );
}

export default Footer;
