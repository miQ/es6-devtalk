import styled from 'styled-components';

export const Table = styled.table`
  width: 100%;
`;

export const Team = styled.div`
  background: white;
  min-width: 150px;
  border: 1px solid grey;
  &:nth-child(2n) {
    margin-bottom: 30px
  }
`;
