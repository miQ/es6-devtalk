import React from 'react';
import {Table, Team} from './tableStyles';
function TournamentBracket(props) {

  const generateMatches = (matches) => {
    return matches.map((match, it) => {
      return (
        <Team key={it}>
          {match.name}
        </Team>
      )
    })
  };

  const generateRounds = (rounds) => {
    return rounds.map((round) => {
      return (
        <td key={round.id}>
          {generateMatches(round.matches)}
        </td>
      )
    })
  };

  const generateHeaders = (rounds) => {
    return rounds.map((round) => {
      return (
        <th key={round.id}>
          <h1>{round.id}</h1>
        </th>
      )
    })
  };

  return (
    <Table>
      <thead>
        <tr>
          {generateHeaders(props.rounds)}
        </tr>
      </thead>
      <tbody>
        <tr>
          {generateRounds(props.rounds)}
        </tr>
      </tbody>
    </Table>
  )
}
TournamentBracket.propTypes = {
  rounds: React.PropTypes.array
};

export default TournamentBracket;