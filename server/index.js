/* eslint consistent-return:0 */

const express = require('express');
const logger = require('./logger');

const argv = require('minimist')(process.argv.slice(2));
const setup = require('./middlewares/frontendMiddleware');
const isDev = process.env.NODE_ENV !== 'production';
const ngrok = (isDev && process.env.ENABLE_TUNNEL) || argv.tunnel ? require('ngrok') : false;
const resolve = require('path').resolve;
const app = express();
const myApi = express.Router();

myApi.get('/teams', (req, res) => {
  res.send({
    teams: [
      {
        id: 0,
        matches: [
          {id: 1, name: 'Brasil'},
          {id: 2, name: 'Poland'},
          {id: 3, name: 'France'},
          {id: 3, name: 'France'},
          {id: 3, name: 'France'},
          {id: 3, name: 'France'},
          {id: 3, name: 'France'},
          {id: 3, name: 'France'},
          {id: 1, name: 'Brasil'},
          {id: 2, name: 'Poland'},
          {id: 3, name: 'France'},
          {id: 3, name: 'France'},
          {id: 3, name: 'France'},
          {id: 3, name: 'France'},
          {id: 3, name: 'France'},
          {id: 3, name: 'France'},
        ]
      },
      {
        id: 1,
        matches: [
          {id: 1, name: 'Brasil'},
          {id: 2, name: 'Poland'},
          {id: 3, name: 'France'},
          {id: 3, name: 'France'},
          {id: 1, name: 'Brasil'},
          {id: 2, name: 'Poland'},
          {id: 3, name: 'France'},
          {id: 3, name: 'France'}
        ]
      },
      {
        id: 2,
        matches: [
          {id: 1, name: 'Brasil'},
          {id: 2, name: 'Poland'},
          {id: 1, name: 'Brasil'},
          {id: 2, name: 'Poland'},
        ]
      },
      {
        id: 3,
        matches: [
          {id: 1, name: 'Brasil'},
          {id: 2, name: 'Poland'},
        ]
      }
    ]
  });
});

// If you need a backend, e.g. an API, add your custom backend-specific middleware here
app.use('/api', myApi);
// In production we need to pass these values in instead of relying on webpack
setup(app, {
  outputPath: resolve(process.cwd(), 'build'),
  publicPath: '/',
});

// get the intended port number, use port 3000 if not provided
const port = argv.port || process.env.PORT || 3000;

// Start your app.
app.listen(port, (err) => {
  if (err) {
    return logger.error(err.message);
  }

  // Connect to ngrok in dev mode
  if (ngrok) {
    ngrok.connect(port, (innerErr, url) => {
      if (innerErr) {
        return logger.error(innerErr);
      }

      logger.appStarted(port, url);
    });
  } else {
    logger.appStarted(port);
  }
});
